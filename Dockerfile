FROM php:7.4.1-apache
MAINTAINER Aji Prakoso <jipraks@outlook.com>

WORKDIR /home

RUN apt-get update -y
RUN apt-get install -y zip unzip wget
RUN apt install -y graphviz libzip-dev

# RUN apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libpng-dev && docker-php-ext-configure gd --with-freetype --with-jpeg && docker-php-ext-install -j$(nproc) gd
RUN apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libpng-dev && docker-php-ext-configure gd --with-freetype --with-jpeg && docker-php-ext-install -j "$(nproc)" gd
RUN docker-php-ext-configure mysqli && docker-php-ext-install mysqli
RUN docker-php-ext-configure zip && docker-php-ext-install zip
RUN apt-get install -y libxml2-dev && docker-php-ext-configure soap && docker-php-ext-install soap
RUN apt-get install -y libldb-dev libldap2-dev && docker-php-ext-configure ldap && docker-php-ext-install ldap

RUN wget https://nchc.dl.sourceforge.net/project/itop/itop/2.7.1/iTop-2.7.1-5896.zip
RUN unzip iTop-2.7.1-5896.zip
RUN rm -Rf /var/www/html
RUN mv web /var/www/html

VOLUME /var/www/html

EXPOSE 80
